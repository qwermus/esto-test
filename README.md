## Test task for ESTO

Route examples:

http://127.0.0.1:8000/graphql?query=query+users{users(page:1,limit:2){user_id,name,email,permissions,sumCreditTransactions,sumDebitTransactions,transactions{id,amount,type}}}

http://127.0.0.1:8000/graphql?query=mutation+users{createTransaction(amount:12.55,type:"debit"){user_id,name,email,permissions,sumCreditTransactions,sumDebitTransactions,transactions{id,amount,type}}}

http://127.0.0.1:8000/graphql?query=mutation+users{createUser(name:"John",email:"john@gmail.com",password:"abcdefgh",permissions:"customer"){user_id,name,email,permissions}}
