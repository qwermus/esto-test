<?php

return [

    // Authorized user. While we don't have login route,
    // Let's use this id as authorized user
    'authorizedUserId' => 1,

    // Max position in request limit parameter
    'maxLimit' => 200,

    // Max amount of transaction
    'maxAmount' => 9999999,

    // Transaction types
    'transactionTypes' => ['debit', 'credit'],

    // User permissions
    'userPermissions' => ['customer', 'admin'],

    // Min password length
    'minPassword' => 8,
];
