<?php

/**
 * Give permissions to each user type
 */
return [

    // Permissions for customer
    'customer' => [
        'users' => true, // List users
        'createTransaction' => true, // Create transactions
        'createUser' => false, // Create users
    ],

    // Permissions for admin
    'admin' => [
        'users' => true, // List users
        'createTransaction' => true, // Create transactions
        'createUser' => true, // Create users
    ],

    // Permissions for not authorized user
    'none' => [
        'users' => true, // List users
        'createTransaction' => false, // Create transactions
        'createUser' => false, // Create users
    ],
];
