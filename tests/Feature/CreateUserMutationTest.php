<?php

namespace Tests\Feature;

use Str;
use Response;
use Tests\TestCase;
use App\Models\User;
use Tests\Traits\Testable;
use Illuminate\Foundation\Testing\WithFaker;

/**
 * Class CreateUserMutation
 * @package Tests\Feature
 * @author Volodymyr Lvov
 * @date 12.11.2020
 */
class CreateUserMutationTest extends TestCase
{
    /**
     * Use trait for common features
     */
    use Testable;
    use WithFaker;

    /**
     * @var string
     */
    private $query;
    private $randomUser;

    /**
     * Create query with faker
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->query = '
            mutation users{
                createUser(
                    name:"'.$this->faker->name.'",
                    email:"'.$this->faker->unique()->safeEmail.'",
                    password:"'.Str::random(10).'",
                    permissions:"customer"
                )
                {
                    user_id,
                    name,
                    email,
                    permissions
                }
            }';

        // Get some random user from database to use her data for unique testing
        $this->randomUser = User::first();
    }

    /**
     * Test for success
     *
     * @return void
     */
    public function testSuccess()
    {
        // Get response
        $response = $this->getResponse($this->query);

        // Check response for status and data
        $response->assertStatus(200);
        $this->assertArrayHasKey('data', $response->getData(true));
        $this->assertArrayNotHasKey('errors', $response->getData(true));
    }

    /**
     * Get broken name parameter
     *
     * @return void
     */
    public function testFailWhenNameIsInvalid()
    {
        // Get response
        $query = preg_replace('/[\s]+name:"[^"]+"/', 'name:"'.$this->randomUser->name.'"', $this->query);
        $response = $this->getResponse($query);

        // Check response for status and data
        $response->assertStatus(200);
        $this->assertArrayHasKey('errors', $response->getData(true));
    }

    /**
     * Get broken email parameter
     *
     * @return void
     */
    public function testFailWhenEmailIsInvalid()
    {
        // Get response
        $query = preg_replace('/[\s]+email:"[^"]+"/', 'email:"'.$this->randomUser->email.'"', $this->query);
        $response = $this->getResponse($query);

        // Check response for status and data
        $response->assertStatus(200);
        $this->assertArrayHasKey('errors', $response->getData(true));
    }

    /**
     * Get broken password parameter
     *
     * @return void
     */
    public function testFailWhenPasswordIsInvalid()
    {
        // Get response
        $query = preg_replace('/[\s]+password:"[^"]+"/', 'password:"'.Str::random(2).'"', $this->query);
        $response = $this->getResponse($query);

        // Check response for status and data
        $response->assertStatus(200);
        $this->assertArrayHasKey('errors', $response->getData(true));
    }

    /**
     * Get broken permissions parameter
     *
     * @return void
     */
    public function testFailWhenPermissionsIsInvalid()
    {
        // Get response
        $query = preg_replace('/[\s]+permissions:"[^"]+"/', 'permissions:"'.Str::random(10).'"', $this->query);
        $response = $this->getResponse($query);

        // Check response for status and data
        $response->assertStatus(200);
        $this->assertArrayHasKey('errors', $response->getData(true));
    }
}
