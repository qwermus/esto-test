<?php

namespace Tests\Feature;

use Response;
use Tests\TestCase;
use Tests\Traits\Testable;

/**
 * Class CreateTransactionMutation
 * @package Tests\Feature
 * @author Volodymyr Lvov
 * @date 12.11.2020
 */
class CreateTransactionMutationTest extends TestCase
{
    /**
     * Use trait for common features
     */
    use Testable;

    /**
     * @var string
     */
    private $query = '
        mutation users{
            createTransaction(
                amount:12.55,
                type:"debit"
            )
            {
                user_id,
                name,
                sumDebitTransactions,
                sumCreditTransactions
            }
        }';


    /**
     * Test for success
     *
     * @return void
     */
    public function testSuccess()
    {
        // Get response
        $response = $this->getResponse($this->query);

        // Check response for status and data
        $response->assertStatus(200);
        $this->assertArrayHasKey('data', $response->getData(true));
        $this->assertArrayNotHasKey('errors', $response->getData(true));
    }

    /**
     * Get broken name parameter
     *
     * @return void
     */
    public function testFailWhenAmountIsInvalid()
    {
        // Get response
        $query = preg_replace('/[\s]+amount:[\d.]+/', 'amount:0', $this->query);
        $response = $this->getResponse($query);

        // Check response for status and data
        $response->assertStatus(200);
        $this->assertArrayHasKey('errors', $response->getData(true));
    }

    /**
     * Get broken email parameter
     *
     * @return void
     */
    public function testFailWhenTypeIsInvalid()
    {
        // Get response
        $query = preg_replace('/[\s]+type:"[^"]+"/', 'type:"fake"', $this->query);
        $response = $this->getResponse($query);

        // Check response for status and data
        $response->assertStatus(200);
        $this->assertArrayHasKey('errors', $response->getData(true));
    }
}
