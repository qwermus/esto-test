<?php

namespace Tests\Feature;

use Str;
use Tests\TestCase;
use App\Models\User;
use App\Models\Transaction;
use App\Services\UserService;
use Illuminate\Foundation\Testing\WithFaker;

/**
 * Class CreateUserTest
 * @package Tests\Feature
 * @author Volodymyr Lvov
 * @date 16.11.2020
 */
class CreateUserTest extends TestCase
{
    use WithFaker;

    /**
     * Test for success
     *
     * @return void
     */
    public function testSuccess()
    {
        // Fields
        $name = $this->faker->name;
        $email = $this->faker->unique()->safeEmail;
        $password = Str::random(10);

        // Get random permission
        $permissions = config('esto.userPermissions');
        $key = array_rand($permissions);

        /**
         * Create a user
         */
        $user = UserService::createUser($name, $email, $password, $permissions[$key]);
        $this->assertTrue($user instanceof User);

        // Save user to use it later
        $user_id = $user->id;

        /**
         * Select created user
         */
        $user = User::find($user_id);
        $this->assertNotNull($user);

        /**
         * Select transaction creation
         */
        // Get transaction's amount
        $amount = rand(100,99999) / 100;

        $transaction = (new UserService($user))->createTransaction($amount, 'debit');
        $this->assertTrue($transaction instanceOf Transaction);

        /**
         * Get transaction sum from DB
         */
        $user = User::find($user_id);
        $this->assertEquals($user->sumDebitTransactions, $amount);

        /**
         * Destroy user and her transactions
         */
        $user->delete();
        $this->assertNull(User::find($user_id));
        $this->assertNull(Transaction::whereUserId($user_id)->first());
    }
}
