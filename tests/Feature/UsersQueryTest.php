<?php

namespace Tests\Feature;

use Response;
use Tests\TestCase;
use Tests\Traits\Testable;

/**
 * Class UsersQueryTest
 * @package Tests\Feature
 * @author Volodymyr Lvov
 * @date 12.11.2020
 */
class UsersQueryTest extends TestCase
{
    /**
     * Use trait for common features
     */
    use Testable;

    /**
     * @var string
     */
    private $query = '
        query users{
            users(
                page:1,
                limit:2
            )
            {
                user_id,
                name,
                email,
                permissions,
                sumDebitTransactions,
                sumCreditTransactions,
                transactions{
                    id,
                    user_id,
                    amount,
                    type
                }
            }
        }';

    /**
     * Test for success
     *
     * @return void
     */
    public function testSuccess()
    {
        // Get response
        $response = $this->getResponse($this->query);

        // Check response for status and data
        $response->assertStatus(200);
        $this->assertArrayHasKey('data', $response->getData(true));
        $this->assertArrayNotHasKey('errors', $response->getData(true));
    }

    /**
     * Get broken page parameter
     *
     * @return void
     */
    public function testFailWhenPageIsInvalid()
    {
        // Get response
        $query = preg_replace('/[\s]+page:[\d]+/', 'page:"fake"', $this->query);
        $response = $this->getResponse($query);

        // Check response for status and data
        $response->assertStatus(200);
        $this->assertArrayHasKey('errors', $response->getData(true));
    }

    /**
     * Get broken limit parameter
     *
     * @return void
     */
    public function testFailWhenLimitIsInvalid()
    {
        // Get response
        $query = preg_replace('/[\s]+limit:[\d]+/', 'page:"fake"', $this->query);
        $response = $this->getResponse($query);

        // Check response for status and data
        $response->assertStatus(200);
        $this->assertArrayHasKey('errors', $response->getData(true));
    }

    /**
     * Get another broken parameter
     *
     * @return void
     */
    public function testFailWhenSomeParameterIsInvalid()
    {
        $query = preg_replace('/[\s]+permissions,[\s]+/', 'fake,', $this->query);
        $response = $this->getResponse($query);

        // Check response for status and data
        $response->assertStatus(200);
        $this->assertArrayHasKey('errors', $response->getData(true));
    }
}
