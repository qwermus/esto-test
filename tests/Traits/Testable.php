<?php

namespace tests\Traits;

use Auth;
use Response;
use App\Models\User;
use Illuminate\Testing\TestResponse;

/**
 * Trait Testable
 * @package tests\Traits
 * @author Volodymyr Lvov
 * @date 12.11.2020
 */
trait Testable
{
    /**
     * @var Authorized user
     */
    protected $authorized;

    /**
     * Get authorized user
     *
     * @return User
     */
    protected function authorize()
    {
        $this->authorized = User::whereId(config('esto.authorizedUserId', 1))->firstOrFail();

        // Authorize user
        Auth::login($this->authorized);
    }

    /**
     * Get response from server
     *
     * @param string $query
     * @return Response
     */
    protected function getResponse(string $query) : TestResponse
    {
        // First we need to be authorized before starting a test
        $this->authorize();

        // Then get a response
        return $this->call('GET', config('graphql.prefix'), ['query' => $query]);
    }
}
