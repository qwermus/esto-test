<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Create an admin
        User::factory(1)->create(['permissions' => 'admin']);

        // Create other users if needed
        // User::factory(3)->create(['permissions' => 'customer']);
    }
}
