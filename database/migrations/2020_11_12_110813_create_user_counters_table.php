<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCountersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_counters', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->nullable()->unsigned()->comment('User\'s id')->index();

            // We can add more counters if we need.
            // For high-load projects it is better to use triggers to increment and decrement this counters,
            // But for small project or for test it is ok
            $table->integer('count_debit_transactions')->default(0)->unsigned()->comment('Count of debit transactions');
            $table->integer('count_credit_transactions')->default(0)->unsigned()->comment('Count of credit transactions');
            $table->decimal('total_debit_transactions', 10, 2)->default(0)->unsigned()->comment('Total of debit transactions');
            $table->decimal('total_credit_transactions', 10, 2)->default(0)->unsigned()->comment('Total of credit transactions');
            $table->timestamps();

            // Add a foreign key
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_counters');
    }
}
