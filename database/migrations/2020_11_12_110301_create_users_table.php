<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name', 150)->unique()->comment('User\'s name');;
            $table->string('email', 150)->unique()->comment('User\'s unique email');;
            $table->string('password')->comment('User\'s password');
            $table->enum('permissions', ['customer', 'admin'])->default('customer')->comment('User\'s role');
            $table->timestamps();

            /**
             * In real projects usually i would like to use soft deletes for this table
             * In this project we don not destroy users
             */
            // $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
