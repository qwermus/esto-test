<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->nullable()->unsigned()->comment('User\'s id')->index();
            $table->enum('type', ['debit', 'credit'])->default('debit')->comment('Type of the transaction');
            $table->decimal('amount', 10, 2)->default(0)->unsigned()->comment('Amount of the transaction');
            $table->timestamps();

            /**
             * I think, transactions can not be deleted. But, if needed, we can use soft deletes for this
             */
            // $table->softDeletes();

            // Add a foreign key
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
