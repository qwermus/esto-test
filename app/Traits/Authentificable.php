<?php

namespace App\Traits;

use Auth;
use Closure;
use App\Models\User;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Trait Authentificable
 * @package App\Traits
 * @author Volodymyr Lvov
 * @date 12.11.2020
 */
trait Authentificable
{
    /**
     * @var Authorized user
     */
    protected $authorized;

    /**
     * Get authorized user
     *
     * @param $root
     * @param array $args
     * @param $ctx
     * @param ResolveInfo|null $resolveInfo
     * @param Closure|null $getSelectFields
     * @return bool
     */
    public function authorize($root, array $args, $ctx, ResolveInfo $resolveInfo = null, Closure $getSelectFields = null): bool
    {
        // Get authorized user
        if (!is_null(Auth::user())) {
            $this->authorized = Auth::user();
        }

        // May be needed for testing
        // $this->authorized = User::first();

        // Get user permissions
        return $this->getPermission();
    }

    /**
     * Check permissions from config file
     *
     * @return bool
     */
    private function getPermission() : bool
    {
        // User's role
        $permissions = config(implode('.', ['permissions', $this->authorized->permissions ?? 'none']));

        // If there is no role - abort, no permissions
        if (is_null($permissions) || !isset($permissions[$this->attributes['name']])) {
            return false;
        }

        // Check if user has permission to current route
        return $permissions[$this->attributes['name']];
    }
}
