<?php

namespace App\Services;

use DB;
use App\Models\User;
use App\Models\Transaction;

/**
 * Class UserService
 * @package App\Services
 * @author Volodymyr Lvov
 * @date 12.11.2020
 */
class UserService
{
    private $model;

    /**
     * UserService constructor - create a model
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    /**
     * Get latest users with paginator
     *
     * @return object
     */
    public static function getLatestUsers(int $page, int $limit) : object
    {
        return User::latest()->paginate($limit, ['*'], 'page', $page);
    }

    /**
     * Create a user
     *
     * @param $amount
     * @param $type
     */
    public static function createUser(string $name, string $email, string $password, string $permissions) : User
    {
        // Create a user
        return User::create([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password),
            'permissions' => $permissions
        ]);
    }

    /**
     * Create user's transaction
     *
     * @param $amount
     * @param $type
     */
    public function createTransaction(float $amount, string $type) : Transaction
    {
        // Create a transaction
        return $this->model->transactions()->create([
            'amount' => $amount,
            'type' => $type
        ]);
    }
}
