<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class User
 * @package App\Models
 * @author Volodymyr Lvov
 * @date 12.11.2020
 */
class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'permissions'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    /**
     * @param  string  $value
     * @return void
     */
    public function getUserIdAttribute()
    {
        return $this->id;
    }

    /**
     * User has many transactions
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'user_id');
    }

    /**
     * Get total debit transactions
     *
     * @param  string  $value
     * @return void
     */
    public function getSumDebitTransactionsAttribute()
    {
        return $this->transactions()->whereType('debit')->sum('amount');
    }

    /**
     * Get total credit transactions
     *
     * @param  string  $value
     * @return void
     */
    public function getSumCreditTransactionsAttribute()
    {
        return $this->transactions()->whereType('credit')->sum('amount');
    }
}
