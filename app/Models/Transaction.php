<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Transaction
 * @package App\Models
 * @author Volodymyr Lvov
 * @date 12.11.2020
 */
class Transaction extends Model
{
    /**
     * Fillable columns
     */
    protected $fillable = [
        'user_id',
        'type',
        'amount',
    ];

    /**
     * Transaction belongs to a user
     */
    public function user()
    {
        //return $this->belongsTo(User::class, 'user_id');
        return $this->belongsTo(User::class, 'user_id');
    }
}
