<?php

namespace App\GraphQL\Queries;

use Closure;
use App\Models\User;
use App\Services\UserService;
use App\Traits\Authentificable;
use Rebing\GraphQL\Support\Query;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\Facades\GraphQL;

/**
 * Class UsersQuery
 * @package App\GraphQL\Queries
 * @author Volodymyr Lvov
 * @date 12.11.2020
 */
class UsersQuery extends Query
{
    /**
     * Use Authorization and permissions
     */
    use Authentificable;

    /**
     * @var string[]
     */
    protected $attributes = [
        'name' => 'users',
    ];

    /**
     * @return Type
     */
    public function type(): Type
    {
        return Type::listOf(GraphQL::type('user'));
    }

    /**
     * @return array[]
     */
    public function args(): array
    {
        return [
            'page' => ['name' => 'page', 'type' => Type::nonNull(Type::int())],
            'limit' => ['name' => 'limit', 'type' => Type::nonNull(Type::int())],
        ];
    }

    /**
     * Validation rules
     * @return array[]
     */
    protected function rules(array $args = []): array
    {
        return [
            'page' => ['required', 'integer', 'min:1'],
            'limit' => ['required', 'integer', 'min:1', 'max:'.config('esto.maxLimit', 200)]
        ];
    }

    /**
     * @param $root
     * @param $args
     * @param $context
     * @param ResolveInfo $resolveInfo
     * @param Closure $getSelectFields
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        // For high-load project it is better to use here select($select)->with($with),
        // But in small project or in test it is not necessary
        return UserService::getLatestUsers($args['page'], $args['limit']);
    }
}
