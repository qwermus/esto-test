<?php

namespace App\GraphQL\Mutations;

use Closure;
use App\Services\UserService;
use Illuminate\Validation\Rule;
use App\Traits\Authentificable;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\Facades\GraphQL;

/**
 * Class CreateTransactionMutation
 * @package App\GraphQL\Queries
 * @author Volodymyr Lvov
 * @date 12.11.2020
 */
class CreateTransactionMutation extends Mutation
{
    /**
     * Use Authorization and permissions
     */
    use Authentificable;

    /**
     * @var string[]
     */
    protected $attributes = [
        'name' => 'createTransaction',
    ];

    /**
     * @return Type
     */
    public function type(): Type
    {
        return GraphQL::type('user');
    }

    /**
     * @return array[]
     */
    public function args(): array
    {
        return [
            'amount' => ['name' => 'amount', 'type' => Type::nonNull(Type::float())],
            'type' => ['name' => 'type', 'type' => Type::nonNull(Type::string())],
        ];
    }

    /**
     * Validation rules
     *
     * @return array[]
     */
    protected function rules(array $args = []): array
    {
        return [
            'amount' => [
                'required',
                'numeric',
                'min:0.01',
                'max:'.config('esto.maxAmount', 9999999)
            ],
            'type' => [
                'required',
                Rule::in(config('esto.transactionTypes', []))
            ]
        ];
    }

    /**
     * @param $root
     * @param $args
     * @param $context
     * @param ResolveInfo $resolveInfo
     * @param Closure $getSelectFields
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        // We need to create a transaction
        (new UserService($this->authorized))->createTransaction($args['amount'], $args['type']);

        // If ok - return a user
        return $this->authorized;
    }
}
