<?php

namespace App\GraphQL\Mutations;

use Closure;
use App\Services\UserService;
use Illuminate\Validation\Rule;
use App\Traits\Authentificable;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\ResolveInfo;
use Rebing\GraphQL\Support\Facades\GraphQL;

/**
 * Class CreateUserMutation
 * @package App\GraphQL\Queries
 * @author Volodymyr Lvov
 * @date 12.11.2020
 */
class CreateUserMutation extends Mutation
{
    /**
     * Use Authorization and permissions
     */
    use Authentificable;

    /**
     * @var string[]
     */
    protected $attributes = [
        'name' => 'createUser',
    ];

    /**
     * @return Type
     */
    public function type(): Type
    {
        return GraphQL::type('user');
    }

    /**
     * @return array[]
     */
    public function args(): array
    {
        return [
            'name' => ['name' => 'name', 'type' => Type::nonNull(Type::string())],
            'email' => ['name' => 'email', 'type' => Type::nonNull(Type::string())],
            'password' => ['name' => 'password', 'type' => Type::nonNull(Type::string())],
            'permissions' => ['name' => 'permissions', 'type' => Type::nonNull(Type::string())],
        ];
    }

    /**
     * Validation rules
     * @return array[]
     */
    protected function rules(array $args = []): array
    {
        return [
            'name' => [
                'required',
                'string',
                'min:2',
                'max:150',
                'unique:users'
            ],
            'email' => [
                'required',
                'email',
                'max:150',
                'unique:users'
            ],
            'password' => [
                'required',
                'string',
                // Here we can use regular expression here to ask hard password
                'min:' . config('esto.minPassword', 8),
            ],
            'permissions' => [
                'required',
                'string',
                Rule::in(config('esto.userPermissions', []))
            ],
        ];
    }

    /**
     * @param $root
     * @param $args
     * @param $context
     * @param ResolveInfo $resolveInfo
     * @param Closure $getSelectFields
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function resolve($root, $args, $context, ResolveInfo $resolveInfo, Closure $getSelectFields)
    {
        // We need to create a user
        $user = UserService::createUser($args['name'], $args['email'], $args['password'], $args['permissions']);

        // If ok - return a user
        return $user;
    }
}
