<?php

namespace App\GraphQL\Types;

use App\Models\User;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

/**
 * Class UserType
 * @package App\GraphQL\Types
 * @author Volodymyr Lvov
 * @date 12.11.2020
 */
class UserType extends GraphQLType
{
    /**
     * @var string[]
     */
    protected $attributes = [
        'name'          => 'User',
        'description'   => 'A user',
        'model'         => User::class,
    ];

    /**
     * @return array[]
     */
    public function fields(): array
    {
        return [
            'user_id' => [
                'type' => Type::int(),
                'description' => 'The id of the user',
                'selectable' => false,
            ],
            'name' => [
                'type' => Type::string(),
                'description' => 'The name of the user',
            ],
            'email' => [
                'type' => Type::string(),
                'description' => 'The email of the user',
            ],
            'sumDebitTransactions' => [
                'type' => Type::float(),
                'description' => 'The amount of all CU debit transactions',
            ],
            'sumCreditTransactions' => [
                'type' => Type::float(),
                'description' => 'The amount of all CU credit transactions',
            ],
            'permissions' => [
                'type' => Type::string(),
                'description' => 'Type of the user',
            ],
            'transactions' => [
                'type' => Type::listOf(GraphQL::type('transaction')),
                'description' => 'User transactions',
            ]
        ];
    }
}
