<?php

namespace App\GraphQL\Types;

use App\Models\Transaction;
use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

/**
 * Class TransactionType
 * @package App\GraphQL\Types
 * @author Volodymyr Lvov
 * @date 12.11.2020
 */
class TransactionType extends GraphQLType
{
    /**
     * @var string[]
     */
    protected $attributes = [
        'name'          => 'Transaction',
        'description'   => 'A transaction',
        'model'         => Transaction::class,
    ];

    /**
     * @return array[]
     */
    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::int(),
                'description' => 'The id of the transaction',
            ],
            'user_id' => [
                'type' => Type::int(),
                'description' => 'The id of the user',
            ],
            'type' => [
                'type' => Type::string(),
                'description' => 'Type of the transaction',
            ],
            'amount' => [
                'type' => Type::float(),
                'description' => 'Amount of the transaction',
            ],
        ];
    }
}
